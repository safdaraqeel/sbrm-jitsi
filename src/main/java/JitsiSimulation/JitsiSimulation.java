package JitsiSimulation;

import SBRM.CG.DecodedConfigurableParameters;
import Utils.My_Writer;

import java.util.Random;

public class JitsiSimulation
{
    public static void simulate(int totalCalls) {
        try {
            int callTime = 10; // in seconds
            String callerIP = "sip:a@172.16.4.186";
            String peer1IP = "sip:b@172.16.0.159";
            String peer2IP = "sip:c@172.16.5.113";
            for (int i = 0; i < totalCalls; i++)
            {
                long CallID = System.currentTimeMillis();
                DecodedConfigurableParameters peer1Configuration = new DecodedConfigurableParameters();
                peer1Configuration.ReadConfigurationFromCSVFile(peer1IP);

                DecodedConfigurableParameters peer2Configuration = new DecodedConfigurableParameters();
                peer2Configuration.ReadConfigurationFromCSVFile(peer2IP);

                DecodedConfigurableParameters callerConfiguration = new DecodedConfigurableParameters();
                callerConfiguration.ReadConfigurationFromCSVFile(callerIP);
                
                boolean flagCall1 = CallStatusBasedOnRules(callerConfiguration,peer1Configuration);
                boolean flagCall2 = CallStatusBasedOnRules(callerConfiguration,peer2Configuration);
                System.err.println("    "+flagCall1+"-"+flagCall2);
                
                String[] CallStatus =
                { "", "", "" };
                CallStatus[0] = String.valueOf(flagCall1).toLowerCase()
                    .replaceAll("true", "Connected")
                    .replaceAll("false", "Failed");
                CallStatus[1] = String.valueOf(flagCall2).toLowerCase()
                    .replaceAll("true", "Connected")
                    .replaceAll("false", "Failed");
                CallStatus[2] = CallStatus[0] + CallStatus[1];
                long CallID2 = System.currentTimeMillis();
                String[] callIDAndCallMembers =
                { "" + CallID, "" + CallID2, callerIP, peer1IP, peer2IP };
//               if (CallStatus[2].toLowerCase().contains("connected")){
                writeResults(callIDAndCallMembers, callerConfiguration, peer1Configuration, peer2Configuration, CallStatus); // Writing all results
                System.err.println("Iteration:- "+i);
//               }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public static void writeResults(String[] callIDAndCallMembers,
        DecodedConfigurableParameters callerConfiguration,
        DecodedConfigurableParameters peer1Configuration,
        DecodedConfigurableParameters peer2Configuration, String[] CallStatus)
    {
        try {
            String header = "callID;callID2;Caller;Peer1;Peer2;protocol;protocol_s2;protocol_s3;sIPListenPort;sIPListenPort_s2;sIPListenPort_s3;transportProtocol;transportProtocol_s2;transportProtocol_s3;encryption;encryption_s2;encryption_s3;sipZrtpAttribute;sipZrtpAttribute_s2;sipZrtpAttribute_s3;maxPeersAllowedInConferenceCall;maxPeersAllowedInConferenceCall_s2;maxPeersAllowedInConferenceCall_s3;MTU;MTU_s2;MTU_s3;defaultCallRate;defaultCallRate_s2;defaultCallRate_s3;maxReceiveCallRate;maxReceiveCallRate_s2;maxReceiveCallRate_s3;maxTransmitCallRate;maxTransmitCallRate_s2;maxTransmitCallRate_s3;audioCodec;audioCodec_s2;audioCodec_s3;videoCodec;videoCodec_s2;videoCodec_s3;maxResolution;maxResolution_s2;maxResolution_s3;Caller-Peer1;Caller-Peer2;Conference\n";
            String data = "\n" + callIDAndCallMembers[0] + ";"
                + callIDAndCallMembers[1] + ";" + callIDAndCallMembers[2] + ";"
                + callIDAndCallMembers[3] + ";" + callIDAndCallMembers[4] + ";"

                +callerConfiguration.getProtocol() + ";"
                + peer1Configuration.getProtocol() + ";"
                + peer2Configuration.getProtocol() + ";" +

                callerConfiguration.getSIPListenPort() + ";"
                + peer1Configuration.getSIPListenPort() + ";"
                + peer2Configuration.getSIPListenPort() + ";" +

                callerConfiguration.getTransportProtocol() + ";"
                + peer1Configuration.getTransportProtocol() + ";"
                + peer2Configuration.getTransportProtocol() + ";" +

                callerConfiguration.getEncryption() + ";"
                + peer1Configuration.getEncryption() + ";"
                + peer2Configuration.getEncryption() + ";" +

                callerConfiguration.getSipZrtpAttribute() + ";"
                + peer1Configuration.getSipZrtpAttribute() + ";"
                + peer2Configuration.getSipZrtpAttribute() + ";" +

                callerConfiguration.getMaxPeersAllowedInConferenceCall() + ";"
                + peer1Configuration.getMaxPeersAllowedInConferenceCall() + ";"
                + peer2Configuration.getMaxPeersAllowedInConferenceCall() + ";"
                + callerConfiguration.getMTU() + ";" + peer1Configuration.getMTU()
                + ";" + peer2Configuration.getMTU() + ";" +

                callerConfiguration.getDefaultCallRate() + ";"
                + peer1Configuration.getDefaultCallRate() + ";"
                + peer2Configuration.getDefaultCallRate() + ";" +

                callerConfiguration.getMaxReceiveCallRate() + ";"
                + peer1Configuration.getMaxReceiveCallRate() + ";"
                + peer2Configuration.getMaxReceiveCallRate() + ";" +

                callerConfiguration.getMaxTransmitCallRate() + ";"
                + peer1Configuration.getMaxTransmitCallRate() + ";"
                + peer2Configuration.getMaxTransmitCallRate() + ";" +

                callerConfiguration.getAudioCodec() + ";"
                + peer1Configuration.getAudioCodec() + ";"
                + peer2Configuration.getAudioCodec() + ";" +

                callerConfiguration.getVideoCodec() + ";"
                + peer1Configuration.getVideoCodec() + ";"
                + peer2Configuration.getVideoCodec() + ";" +

                callerConfiguration.getMaxResolution() + ";"
                + peer1Configuration.getMaxResolution() + ";"
                + peer2Configuration.getMaxResolution() + ";" +

                CallStatus[0] + ";" + CallStatus[1] + ";" + CallStatus[2];
                My_Writer.write("ConfigurationStatus.csv", data);
        }
        catch (Throwable e)
        {
            My_Writer.write("ExceptionsForResultsWriting.txt", e.getMessage() + "\n" + e + "\n");
        }

    }

    public static boolean CallStatusBasedOnRules(DecodedConfigurableParameters caller,
         DecodedConfigurableParameters peer1){
       
        //**************Rule for call*************
          if (caller.getProtocol().toLowerCase().contains("sip") && peer1.getSIPListenPort().toLowerCase().contains("off")){ // sip-listen port
              return false;
          }if (caller.getEncryption().toLowerCase().contains("on") && peer1.getEncryption().toLowerCase().contains("off")){    // encryption on/off
              return false;
          }if (caller.getEncryption().toLowerCase().contains("off") && peer1.getEncryption().toLowerCase().contains("on")){     // encryption off/on
              return false;
          }if (caller.getProtocol().toLowerCase().contains("sip") && ( (!caller.getTransportProtocol().toLowerCase().contains("auto")) || (!peer1.getTransportProtocol().toLowerCase().contains("auto")) ||(! caller.getTransportProtocol().toLowerCase().equals(peer1.getTransportProtocol().toLowerCase())))){ // transport protocol should be same
              return false;
          }if (caller.getMaxResolution()>peer1.getMaxResolution()){ // MaxResolution of caller should be less than receiver
              Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
              int c= rand.nextInt(4);
              if (c==1){
                  return false;
              }
          }if (caller.getProtocol().toLowerCase().contains("sip") && ((!caller.getSipZrtpAttribute().toLowerCase().equals(peer1.getSipZrtpAttribute().toLowerCase()))||(!caller.getSipZrtpAttribute().toLowerCase().contains("auto"))||(!peer1.getSipZrtpAttribute().toLowerCase().contains("auto")))){ // SipZrtpAttribute should be same for caller and receiver
              return false;
          }if ((caller.getProtocol().toLowerCase().contains("googletalk")||caller.getProtocol().toLowerCase().contains("ippi")||caller.getProtocol().toLowerCase().contains("iptel.org")||caller.getProtocol().toLowerCase().contains("sip"))  && ((peer1.getVideoCodec().toLowerCase().contains("vp8"))||(peer1.getVideoCodec().toLowerCase().contains("ulpfec")))){// GoogleTalk,iPPi, iptel.org, SIP support h264,red,rtx video codec  {,rtx,ulpfec,VP8}
              return false;
          }if (caller.getProtocol().toLowerCase().contains("iptel.org")  && (peer1.getAudioCodec().toLowerCase().contains("telephone-event-80000"))){//  iptel.org does not support telephone-event-80000 audio codec
              return false;
          }if (peer1.getAudioCodec().toLowerCase().contains("amr-wb-16000")||peer1.getAudioCodec().toLowerCase().contains("silk-12000,")||peer1.getAudioCodec().toLowerCase().contains("silk-8000")){// These audio codecs are not supported at the moment with given prootcols
              return false;
          }if (peer1.getVideoCodec().toLowerCase().contains("vp8")){// These video codecs are not supported at the moment with given prootcol
              return false;
          }if (caller.getMTU() > peer1.getMTU()){ // MTU of caller should not be greater than the the receiver
              Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
              int c= rand.nextInt(4);
              if (c==1){
                  return false;
              }
          }if (caller.getDefaultCallRate()>caller.getMaxTransmitCallRate()){ // callrate should be less than maxtransmit call rate
              Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
              int c= rand.nextInt(4);
              if (c==1){
                  return false;
              }
          }if (caller.getDefaultCallRate()>peer1.getMaxReceiveCallRate()){ // callrate should be less than maxreceived call rate of receiver
              Random rand = new Random(System.currentTimeMillis()*System.nanoTime());
              int c= rand.nextInt(4);
              if (c==1){
                  return false;
              }
          }
        return true;
    }
    
}
