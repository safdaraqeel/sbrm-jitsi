package RandomGeneration;

import SBRM.CG.EncodedConfigurableParameters;
import Utils.My_Writer;

public class GeneratePureRandomConfiguration {

	public static void getPureRandomConfiguration(String path, int totalConfigurations) {
		//int totalConfigurations = 100;
		EncodedConfigurableParameters[] configurations = new EncodedConfigurableParameters[totalConfigurations];
		String ConfigurationData = "";
		for (int i = 0; i < configurations.length; i++) {
			configurations[i] = new EncodedConfigurableParameters();
			configurations[i].getRandomConfiguration();

			ConfigurationData += configurations[i].getProtocol() + " ";
			ConfigurationData += configurations[i].getProtocol_s2() + " ";
			ConfigurationData += configurations[i].getProtocol_s3() + " ";
			ConfigurationData += configurations[i].getSIPListenPort() + " ";
			ConfigurationData += configurations[i].getSIPListenPort_s2() + " ";
			ConfigurationData += configurations[i].getSIPListenPort_s3() + " ";
			ConfigurationData += configurations[i].getTransportProtocol() + " ";
			ConfigurationData += configurations[i].getTransportProtocol_s2() + " ";
			ConfigurationData += configurations[i].getTransportProtocol_s3() + " ";
			ConfigurationData += configurations[i].getEncryption() + " ";
			ConfigurationData += configurations[i].getEncryption_s2() + " ";
			ConfigurationData += configurations[i].getEncryption_s3() + " ";
			ConfigurationData += configurations[i].getSipZrtpAttribute() + " ";
			ConfigurationData += configurations[i].getSipZrtpAttribute_s2() + " ";
			ConfigurationData += configurations[i].getSipZrtpAttribute_s3() + " ";
			ConfigurationData += configurations[i].getMaxPeersAllowedInConferenceCall() + " ";
			ConfigurationData += configurations[i].getMaxPeersAllowedInConferenceCall_s2() + " ";
			ConfigurationData += configurations[i].getMaxPeersAllowedInConferenceCall_s3() + " ";
			ConfigurationData += configurations[i].getMTU() + " ";
			ConfigurationData += configurations[i].getMTU_s2() + " ";
			ConfigurationData += configurations[i].getMTU_s3() + " ";
			ConfigurationData += configurations[i].getDefaultCallRate() + " ";
			ConfigurationData += configurations[i].getDefaultCallRate_s2() + " ";
			ConfigurationData += configurations[i].getDefaultCallRate_s3() + " ";
			ConfigurationData += configurations[i].getMaxReceiveCallRate() + " ";
			ConfigurationData += configurations[i].getMaxReceiveCallRate_s2() + " ";
			ConfigurationData += configurations[i].getMaxReceiveCallRate_s3() + " ";
			ConfigurationData += configurations[i].getMaxTransmitCallRate() + " ";
			ConfigurationData += configurations[i].getMaxTransmitCallRate_s2() + " ";
			ConfigurationData += configurations[i].getMaxTransmitCallRate_s3() + " ";
			ConfigurationData += configurations[i].getAudioCodec() + " ";
			ConfigurationData += configurations[i].getAudioCodec_s2() + " ";
			ConfigurationData += configurations[i].getAudioCodec_s3() + " ";
			ConfigurationData += configurations[i].getVideoCodec() + " ";
			ConfigurationData += configurations[i].getVideoCodec_s2() + " ";
			ConfigurationData += configurations[i].getVideoCodec_s3() + " ";
			ConfigurationData += configurations[i].getMaxResolution() + " ";
			ConfigurationData += configurations[i].getMaxResolution_s2() + " ";
			ConfigurationData += configurations[i].getMaxResolution_s3() + "\n";
		}

		// String header="desktopSharingSettings;desktopSharingSettings_s2;desktopSharingSettings_s3;remoteAccessSettings;remoteAccessSettings_s2;remoteAccessSettings_s3;protocol;protocol_s2;protocol_s3;sIPListenPort;sIPListenPort_s2;sIPListenPort_s3;transportProtocol;transportProtocol_s2;transportProtocol_s3;availabilityStatus;availabilityStatus_s2;availabilityStatus_s3;encryption;encryption_s2;encryption_s3;sipZrtpAttribute;sipZrtpAttribute_s2;sipZrtpAttribute_s3;receivedCallType;receivedCallType_s2;receivedCallType_s3;maxPeersAllowedInConferenceCall;maxPeersAllowedInConferenceCall_s2;maxPeersAllowedInConferenceCall_s3;MTU;MTU_s2;MTU_s3;defaultCallRate;defaultCallRate_s2;defaultCallRate_s3;maxReceiveCallRate;maxReceiveCallRate_s2;maxReceiveCallRate_s3;maxTransmitCallRate;maxTransmitCallRate_s2;maxTransmitCallRate_s3;audioCodec;audioCodec_s2;audioCodec_s3;videoCodec;videoCodec_s2;videoCodec_s3;maxResolution;maxResolution_s2;maxResolution_s3\n";
		My_Writer.write(path, ConfigurationData);
	}
}
