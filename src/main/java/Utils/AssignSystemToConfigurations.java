package Utils;

import java.io.*;
import java.util.Scanner;

public class AssignSystemToConfigurations {

	public static void assignSystemNames(String [] systemNames, String sourceFilePath, String destinationFilePath ){
		String configurations = "";
		
		File sourceFile = new File(sourceFilePath);
		File destinationFile = new File(destinationFilePath);
		try {
			Scanner scan = new Scanner(sourceFile);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String [] Variables=line.split(";");
				String []systemConfigurations={"","",""};
				for (int i = 0; i < Variables.length; i++) {
					systemConfigurations[i % 3]+=Variables[i]+";";
				}
				systemConfigurations[0]+=systemNames[0]+";NotDone;\n";
				systemConfigurations[1]+=systemNames[1]+";NotDone;\n";
				systemConfigurations[2]+=systemNames[2]+";NotDone;\n";
				if (!systemConfigurations[0].contains("protocol")){
					configurations+=systemConfigurations[0]+systemConfigurations[1]+systemConfigurations[2];
				}
			}
			
	    	// writing again
	    	FileWriter fw = new FileWriter(destinationFile,true);
	    	BufferedWriter bw = new BufferedWriter(fw);
	    	bw.write(configurations);
	    	bw.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	
	
	
	
}
