package Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class CovertConfigurationStatusFileToThreeSystemConfigurationFile {

 public static void convert(String sourceFilePath, String destinationFilePath){
//     String header ="callID;callID2;Caller;Peer1;Peer2;protocol;protocol_s2;protocol_s3;sIPListenPort;sIPListenPort_s2;sIPListenPort_s3;transportProtocol;transportProtocol_s2;transportProtocol_s3;encryption;encryption_s2;encryption_s3;sipZrtpAttribute;sipZrtpAttribute_s2;sipZrtpAttribute_s3;maxPeersAllowedInConferenceCall;maxPeersAllowedInConferenceCall_s2;maxPeersAllowedInConferenceCall_s3;MTU;MTU_s2;MTU_s3;defaultCallRate;defaultCallRate_s2;defaultCallRate_s3;maxReceiveCallRate;maxReceiveCallRate_s2;maxReceiveCallRate_s3;maxTransmitCallRate;maxTransmitCallRate_s2;maxTransmitCallRate_s3;audioCodec;audioCodec_s2;audioCodec_s3;videoCodec;videoCodec_s2;videoCodec_s3;maxResolution;maxResolution_s2;maxResolution_s3;Caller-Peer1;Caller-Peer2;Conference\n";
     try
     {
         String configurationsOfThreeSystems="";
         File file = new File(sourceFilePath);
         BufferedReader br = new BufferedReader(new FileReader(file));
         String line = null;
         boolean readflag= false;
         while ((line = br.readLine()) != null) {
             String [] configurationArray =line.split(";");
             String A ="", B ="", C ="";
             for (int i = 5; i < configurationArray.length-3; i++) {
				if (i%3==2){ // system A
					A+=configurationArray[i]+";";
				}
				if (i%3==0){ // system B
					B+=configurationArray[i]+";";
				}
				if (i%3==1){ // system C
					C+=configurationArray[i]+";";
				}
			}
             configurationsOfThreeSystems+=A+"sip:a@172.16.4.186;NotDone\n";
             configurationsOfThreeSystems+=B+"sip:b@172.16.0.159;NotDone\n";
             configurationsOfThreeSystems+=C+"sip:c@172.16.5.113;NotDone\n";
         }
       My_Writer.write(destinationFilePath, configurationsOfThreeSystems);
         br.close();
         System.out.println("File is Converted");
     }
     catch (Exception e)
     {
     }
 }
	public static void main(String[] args) {
		convert("AllFiles/ConfigurationStatus.csv", "AllFiles/ConfigurationsForThreeSystem.csv");
	}
}
