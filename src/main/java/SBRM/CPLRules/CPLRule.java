package SBRM.CPLRules;

public class CPLRule {
    private String expression;
    private boolean result;
    private double support;
    private double violate;
    private String statusType;
    private double distance;
    private double confidence;
    private String cluster;

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public double getSupport() {
        return support;
    }

    public void setSupport(double support) {
        this.support = support;
    }

    public double getViolate() {
        return violate;
    }

    public void setViolate(double violate) {
        this.violate = violate;
    }

    public void printRule() {
        System.out.println("Exp: " + this.getExpression() + ", StatusType: " + this.getStatusType() + " {Support: " + this.getSupport()
                + ", Violation: " + this.getViolate() + ", TotalDataUsedt: " + (this.getSupport() + this.getViolate()) + ", Distance: " + this.getDistance() + ", confidence: " + (this.getSupport() - this.getViolate()) / (this.getSupport() + this.getViolate()) + "}");
    }

    public void printConstraintResult() {
        System.out.println("Exp: " + this.getExpression() + " Result: " + this.getResult());
    }
}
