package SBRM.CG;

import SBRM.CPLRules.CPLRule;

public class Objective3 {
    public static double calculateFitnessForNearConnectedLowConfidence(CPLRule[] rules, EncodedConfigurableParameters configuration){
        double sumOfWeightedDistance=0;
        double maxWeightedDistance=0;

        for (int i=0; i<rules.length;i++){
            if (rules[i].getStatusType().contains("ConnectedConnected") && ( rules[i].getCluster().contains("Low") || rules[i].getCluster().contains("Medium")) ){
                //System.out.println(rules[i].getExpression()+" {Distance: "+rules[i].getDistance()+"}");
                double weightedDistance = (rules[i].getConfidence()* (1-rules[i].getDistance()));
                sumOfWeightedDistance += weightedDistance;
                //calculating the maximum weight by add all weights for all connected status rules
                maxWeightedDistance+=rules[i].getConfidence();
            }// end of if
        }//end of for
        //System.out.println("sumOfWeightedDistance is:- "+sumOfWeightedDistance);
        //System.out.println("maxWeightedDistance is:- "+maxWeightedDistance);
        double normalizedSumOfWeightedDistance = (double)(sumOfWeightedDistance/maxWeightedDistance);	//(x-min/max-min) normalizaation function
        //double normalizedSumOfWeightedDistance = sumOfWeightedDistance/(sumOfWeightedDistance+1); //(x/x+1) normalizaation function
        //System.out.println("normalizedSumOfWeightedDistance is:- "+normalizedSumOfWeightedDistance);
        double fitnessValue = 1-normalizedSumOfWeightedDistance;
        System.out.println("fitness value for exploring near low confidence normal state rules is:- "+fitnessValue);
        return fitnessValue;
    }

}
