package SBRM.CG;

import SBRM.CPLRules.CPLRule;

public class DistanceCalculation {

	// for normalization
	public double nor(double x) {
		return x / (x + 1);
	}

	// for x==y
	public double xEqualsY(double k, double x, double y) {
		// for categorical variables
		if (x < 5) {
			if (x - y == 0) {
				return 0;
			} else {
				return k * nor(Math.abs(x - y));
			}
		}
		// for numeric variables
		else {

			if (x - y == 0) {
				return 0;
			} else {
				return k * nor(Math.abs(x - y));
			}

		}

	}

	// for x!=y
	public double xNotEqualsY(double k, double x, double y) {

		if (x - y != 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x<y
	public double xLessThanY(double k, double x, double y) {

		if (x - y < 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x<=y
	public double xLessEqualsThanY(double k, double x, double y) {

		if (x - y <= 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x>y
	public double xGreaterThanY(double k, double x, double y) {

		if (y - x < 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	// for x>=y
	public double xGreaterEqualsThanY(double k, double x, double y) {

		if (y - x <= 0) {
			return 0;
		} else {
			return k * nor(Math.abs(x - y) + 1);
		}
	}

	
	public CPLRule[] CalculateDistanceForAllRules(CPLRule[] rules, EncodedConfigurableParameters configuration){
		int maximumClausesInAnyRule=0;
		double [] distance = new double [rules.length];
		//System.out.println("This is parsing constraints and calculating distance");
		DistanceCalculation disObj= new DistanceCalculation();
		
		for (int k=0;k<rules.length;k++){
			String [] clauses =rules[k].getExpression().split("AND");
			if (clauses.length>maximumClausesInAnyRule){
				maximumClausesInAnyRule=clauses.length;
			}
		}
		
		
		for (int i=0;i<rules.length;i++){
		String [] clauses =rules[i].getExpression().split("AND");
		for (int j=0;j<clauses.length;j++){
			
			//System.out.println("Rule#"+i+" and clause#"+j+" "+clauses[j]);
			//***********Replacing values and variables in constraintst with numeric number from the configuration data generated ****************
			if (clauses[j].contains("protocol_s3")||clauses[j].contains("protocol_s2")||clauses[j].contains("protocol")){
			clauses[j]=clauses[j].replace("protocol_s3", Double.toString(configuration.getProtocol_s3()));
			clauses[j]=clauses[j].replace("protocol_s2", Double.toString(configuration.getProtocol_s2()));
			clauses[j]=clauses[j].replace("protocol", Double.toString(configuration.getProtocol()));
			clauses[j]=clauses[j].replace("SIP", "0");
			clauses[j]=clauses[j].replace("AIM", "1");
			clauses[j]=clauses[j].replace("GoogleTalk", "2");
			clauses[j]=clauses[j].replace("ICQ", "3");
			clauses[j]=clauses[j].replace("iPPi", "4");
			clauses[j]=clauses[j].replace("iptel.org", "5");
			clauses[j]=clauses[j].replace("IRC", "6");
			}

			if (clauses[j].contains("sIPListenPort_s3")||clauses[j].contains("sIPListenPort_s2")||clauses[j].contains("sIPListenPort")){
			clauses[j]=clauses[j].replace("sIPListenPort_s3", Double.toString(configuration.getSIPListenPort_s3()));
			clauses[j]=clauses[j].replace("sIPListenPort_s2", Double.toString(configuration.getSIPListenPort_s2()));
			clauses[j]=clauses[j].replace("sIPListenPort", Double.toString(configuration.getSIPListenPort()));
			clauses[j]=clauses[j].replace("On", "0");
			clauses[j]=clauses[j].replace("Off", "1");
			}
	
			if (clauses[j].contains("transportProtocol_s3")||clauses[j].contains("transportProtocol_s2")||clauses[j].contains("transportProtocol")){
			clauses[j]=clauses[j].replace("transportProtocol_s3", Double.toString(configuration.getTransportProtocol_s3()));
			clauses[j]=clauses[j].replace("transportProtocol_s2", Double.toString(configuration.getTransportProtocol_s2()));
			clauses[j]=clauses[j].replace("transportProtocol", Double.toString(configuration.getTransportProtocol()));
			clauses[j]=clauses[j].replace("Auto", "0");
			clauses[j]=clauses[j].replace("UDP", "1");
			clauses[j]=clauses[j].replace("TCP", "2");
			clauses[j]=clauses[j].replace("TLS", "3");
			}

			if (clauses[j].contains("encryption_s3")||clauses[j].contains("encryption_s2")||clauses[j].contains("encryption")){
			clauses[j]=clauses[j].replace("encryption_s3", Double.toString(configuration.getEncryption_s3()));
			clauses[j]=clauses[j].replace("encryption_s2", Double.toString(configuration.getEncryption_s2()));
			clauses[j]=clauses[j].replace("encryption", Double.toString(configuration.getEncryption()));
			clauses[j]=clauses[j].replace("On", "0");
			clauses[j]=clauses[j].replace("Off", "1");
			clauses[j]=clauses[j].replace("BestEffort", "2");
			}	

			if (clauses[j].contains("sipZrtpAttribute_s3")||clauses[j].contains("sipZrtpAttribute_s2")||clauses[j].contains("sipZrtpAttribute")){
			clauses[j]=clauses[j].replace("sipZrtpAttribute_s3", Double.toString(configuration.getSipZrtpAttribute_s3()));
			clauses[j]=clauses[j].replace("sipZrtpAttribute_s2", Double.toString(configuration.getSipZrtpAttribute_s2()));
			clauses[j]=clauses[j].replace("sipZrtpAttribute", Double.toString(configuration.getSipZrtpAttribute()));
			clauses[j]=clauses[j].replace("Auto", "0");
			clauses[j]=clauses[j].replace("True", "1");
			clauses[j]=clauses[j].replace("False", "2");
			clauses[j]=clauses[j].replace("TRUE", "1");
			clauses[j]=clauses[j].replace("FALSE", "2");
			}

			//  maxPeersAllowedInConferenceCall 
			if (clauses[j].contains("maxPeersAllowedInConferenceCall_s3")||clauses[j].contains("maxPeersAllowedInConferenceCall_s2")||clauses[j].contains("maxPeersAllowedInConferenceCall")){
			clauses[j]=clauses[j].replace("maxPeersAllowedInConferenceCall_s3", Double.toString(configuration.getMaxPeersAllowedInConferenceCall_s3()));
			clauses[j]=clauses[j].replace("maxPeersAllowedInConferenceCall_s2", Double.toString(configuration.getMaxPeersAllowedInConferenceCall_s2()));
			clauses[j]=clauses[j].replace("maxPeersAllowedInConferenceCall", Double.toString(configuration.getMaxPeersAllowedInConferenceCall()));
			clauses[j]=clauses[j].replace("2", "0");
			clauses[j]=clauses[j].replace("3", "1");
			clauses[j]=clauses[j].replace("4", "2");
			clauses[j]=clauses[j].replace("5", "3");
			}
			
			//	MTU 
			clauses[j]=clauses[j].replace("MTU_s3",  Double.toString(configuration.getMTU_s3()));
			clauses[j]=clauses[j].replace("MTU_s2",  Double.toString(configuration.getMTU_s2()));
			clauses[j]=clauses[j].replace("MTU",  Double.toString(configuration.getMTU_s3()));
			
			//getDefaultCallRate
			clauses[j]=clauses[j].replace("defaultCallRate_s3",  Double.toString(configuration.getDefaultCallRate_s3()));
			clauses[j]=clauses[j].replace("defaultCallRate_s2",  Double.toString(configuration.getDefaultCallRate_s2()));
			clauses[j]=clauses[j].replace("defaultCallRate",  Double.toString(configuration.getDefaultCallRate()));
			
			// getMaxReceiveCallRate
			clauses[j]=clauses[j].replace("maxReceiveCallRate_s3",  Double.toString(configuration.getMaxReceiveCallRate_s3()));
			clauses[j]=clauses[j].replace("maxReceiveCallRate_s2",  Double.toString(configuration.getMaxReceiveCallRate_s2()));
			clauses[j]=clauses[j].replace("maxReceiveCallRate",  Double.toString(configuration.getMaxReceiveCallRate()));

			//getMaxTransmitCallRate
			clauses[j]=clauses[j].replace("maxTransmitCallRate_s3",  Double.toString(configuration.getMaxTransmitCallRate_s3()));
			clauses[j]=clauses[j].replace("maxTransmitCallRate_s2",  Double.toString(configuration.getMaxTransmitCallRate_s2()));
			clauses[j]=clauses[j].replace("maxTransmitCallRate",  Double.toString(configuration.getMaxTransmitCallRate()));

			// AUDIO CODEC
			if (clauses[j].contains("audioCodec_s3")||clauses[j].contains("audioCodec_s2")||clauses[j].contains("audioCodec")){
			clauses[j]=clauses[j].replace("audioCodec_s3", Double.toString(configuration.getAudioCodec_s3()));
			clauses[j]=clauses[j].replace("audioCodec_s2", Double.toString(configuration.getAudioCodec_s2()));
			clauses[j]=clauses[j].replace("audioCodec", Double.toString(configuration.getAudioCodec()));
			clauses[j]=clauses[j].replace("GSM-8000", "10");
			clauses[j]=clauses[j].replace("speex-8000", "11");
			clauses[j]=clauses[j].replace("AMR-WB-16000", "12");
			clauses[j]=clauses[j].replace("SILK-12000", "13");
			clauses[j]=clauses[j].replace("SILK-8000", "14");
			clauses[j]=clauses[j].replace("telephone-event-80000", "15");
			clauses[j]=clauses[j].replace("Auto", "0");
			clauses[j]=clauses[j].replace("opus-48000", "1");
			clauses[j]=clauses[j].replace("SILK-24000", "2");
			clauses[j]=clauses[j].replace("SILK-16000", "3");
			clauses[j]=clauses[j].replace("G722-16000", "4");
			clauses[j]=clauses[j].replace("speex-32000", "5");
			clauses[j]=clauses[j].replace("speex-16000", "6");
			clauses[j]=clauses[j].replace("PCMU-8000", "7");
			clauses[j]=clauses[j].replace("PCMA-8000", "8");
			clauses[j]=clauses[j].replace("iLBC-8000", "9");

			}
			// private String videoCodec
			if (clauses[j].contains("videoCodec_s3")||clauses[j].contains("videoCodec_s2")||clauses[j].contains("videoCodec")){
			clauses[j]=clauses[j].replace("videoCodec_s3", Double.toString(configuration.getVideoCodec_s3()));
			clauses[j]=clauses[j].replace("videoCodec_s2", Double.toString(configuration.getVideoCodec_s2()));
			clauses[j]=clauses[j].replace("videoCodec", Double.toString(configuration.getVideoCodec()));
			clauses[j]=clauses[j].replace("Auto", "0");
			clauses[j]=clauses[j].replace("h264", "1");
			clauses[j]=clauses[j].replace("red", "2");
			clauses[j]=clauses[j].replace("rtx", "3");
			clauses[j]=clauses[j].replace("ulpfec", "4");
			clauses[j]=clauses[j].replace("VP8", "5");
			}
			
			// private String maxResolution
			if (clauses[j].contains("maxResolution_s3")||clauses[j].contains("maxResolution_s2")||clauses[j].contains("maxResolution")){
			clauses[j]=clauses[j].replace("maxResolution_s3", Double.toString(configuration.getMaxResolution_s3()));
			clauses[j]=clauses[j].replace("maxResolution_s2", Double.toString(configuration.getMaxResolution_s2()));
			clauses[j]=clauses[j].replace("maxResolution", Double.toString(configuration.getMaxResolution()));
			clauses[j]=clauses[j].replace("1080", "0");
			clauses[j]=clauses[j].replace("720", "1");
			clauses[j]=clauses[j].replace("480", "2");
			clauses[j]=clauses[j].replace("360", "3");
			clauses[j]=clauses[j].replace("240", "4");
			}
			
			if (clauses[j].contains("<=")){
			//	System.out.println("<= operator found");
				String [] oprands = clauses[j].split("<=");
			//	System.out.println(oprands[0]+" <= " + oprands[1]);
				
				distance[i]+= disObj.xLessEqualsThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
			//	System.out.println("distance is:- "+disObj.xLessEqualsThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
			}
			else if (clauses[j].contains(">=")){
				//System.out.println(">= operator found");
				String [] oprands = clauses[j].split(">=");
				//System.out.println(oprands[0]+" >= " + oprands[1]);
				
				distance[i]+= disObj.xGreaterEqualsThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
			//	System.out.println("distance is:- "+disObj.xGreaterEqualsThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
			}
			else if (clauses[j].contains("<")){
				//System.out.println("< operator found");
				String [] oprands = clauses[j].split("<");
				//System.out.println(oprands[0]+" < " + oprands[1]);
				
				distance[i]+= disObj.xLessThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
			//	System.out.println("distance is:- "+disObj.xLessThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
			}
			else if (clauses[j].contains(">")){
				//System.out.println("> operator found");
				String [] oprands = clauses[j].split(">");
				//System.out.println(oprands[0]+" > " + oprands[1]);
				distance[i]+= disObj.xGreaterThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
			//	System.out.println("distance is:- "+disObj.xGreaterThanY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
			}
			
			else if (clauses[j].contains("=")){
				//System.out.println("= operator found");
				String [] oprands = clauses[j].split("=");
				//System.out.println(oprands[0]+" = " + oprands[1]);
					distance[i]+= disObj.xEqualsY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1]));
			//		System.out.println("distance is:- "+disObj.xEqualsY(1, Double.parseDouble(oprands[0]), Double.parseDouble(oprands[1])));
			}
			else {
				System.out.println("No operator found");
			}
		}
		//constraints[i].setDistance((distance[i]/clauses.length));
		rules[i].setDistance((distance[i]/maximumClausesInAnyRule));
		//System.out.println("The total distance for a constraint"+constraints[i].getExpression()+" is:- "+constraints[i].getDistance());
		}
	return rules;

	}
}
