package SBRM.CG;

import SBRM.CPLRules.CPLRule;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Utility {
	
	public int calculatedistance(String expression, String operator){
		    int occurrences = 0;
		    if (expression.contains(operator)) {
		        int withSentenceLength    = expression.length();
		        int withoutSentenceLength = expression.replaceAll(operator, "").length();
		        occurrences = (withSentenceLength - withoutSentenceLength) / operator.length();
		    }
		    return occurrences+1;
	}

	public static double [] getMaxValue(CPLRule[] rules) {
		Double [] weights = new Double [rules.length];
	    for (int i = 1; i < rules.length; i++) {
	    	weights[i] = rules[i].getSupport() / (rules[i].getSupport()+rules[i].getViolate());
	    }
	
	   List<Double> list= Arrays.asList(weights);
	   Collections.sort(list);
	    double [] minMax= {0,0};
	    minMax[0] = list.get(0);
	    minMax[1] = list.get(list.size());
	    return  minMax;
	}
	    
	
}
