package SBRM.CG;

import java.io.*;
import java.util.Scanner;

public class Decoding {
    public static void ReadData(String sourcePath, String path) throws IOException
    {
    	BufferedWriter writer = new BufferedWriter(new FileWriter(path, true));
    	writer.write("protocol;protocol_s2;protocol_s3;sIPListenPort;sIPListenPort_s2;sIPListenPort_s3;transportProtocol;transportProtocol_s2;transportProtocol_s3;encryption;encryption_s2;encryption_s3;sipZrtpAttribute;sipZrtpAttribute_s2;sipZrtpAttribute_s3;maxPeersAllowedInConferenceCall;maxPeersAllowedInConferenceCall_s2;maxPeersAllowedInConferenceCall_s3;MTU;MTU_s2;MTU_s3;defaultCallRate;defaultCallRate_s2;defaultCallRate_s3;maxReceiveCallRate;maxReceiveCallRate_s2;maxReceiveCallRate_s3;maxTransmitCallRate;maxTransmitCallRate_s2;maxTransmitCallRate_s3;audioCodec;audioCodec_s2;audioCodec_s3;videoCodec;videoCodec_s2;videoCodec_s3;maxResolution;maxResolution_s2;maxResolution_s3\n");
    	String filePath = sourcePath;
		String text = "";
		File fileName = new File(filePath);
		try {
			Scanner scan = new Scanner(fileName);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				String[] Variables = line.split(" ");
				System.out.println(line);
					
				// Conference_DefaultCall_Protocol;
				if (Variables[0].contains("0"))
					writer.write("SIP;");
				if (Variables[0].contains("1"))
					writer.write("AIM;");
				if (Variables[0].contains("2"))
					writer.write("GoogleTalk;");		
				if (Variables[0].contains("3"))
					writer.write("ICQ;");						
				if (Variables[0].contains("4"))
					writer.write("iPPi;");	
				if (Variables[0].contains("5"))
					writer.write("iptel.org;");	
				if (Variables[0].contains("6"))
					writer.write("IRC;");	
					
				if (Variables[1].contains("0"))
					writer.write("SIP;");
				if (Variables[1].contains("1"))
					writer.write("AIM;");
				if (Variables[1].contains("2"))
					writer.write("GoogleTalk;");		
				if (Variables[1].contains("3"))
					writer.write("ICQ;");						
				if (Variables[1].contains("4"))
					writer.write("iPPi;");	
				if (Variables[1].contains("5"))
					writer.write("iptel.org;");	
				if (Variables[1].contains("6"))
					writer.write("IRC;");	
					
				if (Variables[2].contains("0"))
					writer.write("SIP;");
				if (Variables[2].contains("1"))
					writer.write("AIM;");
				if (Variables[2].contains("2"))
					writer.write("GoogleTalk;");		
				if (Variables[2].contains("3"))
					writer.write("ICQ;");						
				if (Variables[2].contains("4"))
					writer.write("iPPi;");	
				if (Variables[2].contains("5"))
					writer.write("iptel.org;");	
				if (Variables[2].contains("6"))
					writer.write("IRC;");	
					
					// Sip_ListenPort {On, Off}
				if (Variables[3].contains("0"))
					writer.write("On;");
				if (Variables[3].contains("1"))
					writer.write("Off;");
					
				if (Variables[4].contains("0"))
					writer.write("On;");
				if (Variables[4].contains("1"))
					writer.write("Off;");
					
				if (Variables[5].contains("0"))
					writer.write("On;");
				if (Variables[5].contains("1"))
					writer.write("Off;");
										
					// Sip_DefaultTransport  {Auto, UDP,TCP,TLS}
				if (Variables[6].contains("0"))
					writer.write("Auto;");
				if (Variables[6].contains("1"))
					writer.write("UDP;");
				if (Variables[6].contains("2"))
					writer.write("TCP;");
				if (Variables[6].contains("3"))
					writer.write("TLS;");
					
				if (Variables[7].contains("0"))
					writer.write("Auto;");
				if (Variables[7].contains("1"))
					writer.write("UDP;");
				if (Variables[7].contains("2"))
					writer.write("TCP;");
				if (Variables[7].contains("3"))
					writer.write("TLS;");
					
				if (Variables[8].contains("0"))
					writer.write("Auto;");
				if (Variables[8].contains("1"))
					writer.write("UDP;");
				if (Variables[8].contains("2"))
					writer.write("TCP;");
				if (Variables[8].contains("3"))
					writer.write("TLS;");
					
					// Conference_Encryption= new Int(0,2);// {On, Off, BestEffort}
				if (Variables[9].contains("0"))
					writer.write("On;");
				if (Variables[9].contains("1"))
					writer.write("Off;");
				if (Variables[9].contains("2"))
					writer.write("BestEffort;");

				if (Variables[10].contains("0"))
					writer.write("On;");
				if (Variables[10].contains("1"))
					writer.write("Off;");
				if (Variables[10].contains("2"))
					writer.write("BestEffort;");
					
				if (Variables[11].contains("0"))
					writer.write("On;");
				if (Variables[11].contains("1"))
					writer.write("Off;");
				if (Variables[11].contains("2"))
					writer.write("BestEffort;");
					
					// private String sipZrtpAttribute = new (0,1) ; //{true, false}
				if (Variables[12].contains("0"))
					writer.write("Auto;");
				if (Variables[12].contains("1"))
					writer.write("TRUE;");
				if (Variables[12].contains("2"))
					writer.write("FALSE;");

				if (Variables[13].contains("0"))
					writer.write("Auto;");
				if (Variables[13].contains("1"))
					writer.write("TRUE;");
				if (Variables[13].contains("2"))
					writer.write("FALSE;");
				
				if (Variables[14].contains("0"))
					writer.write("Auto;");
				if (Variables[14].contains("1"))
					writer.write("TRUE;");
				if (Variables[14].contains("2"))
					writer.write("FALSE;");

					// private int maxPeersAllowedInConferenceCall = new (0,3) ; // {2,3,4,5}
				if (Variables[15].contains("0"))
					writer.write("2;");
				if (Variables[15].contains("1"))
					writer.write("3;");
				if (Variables[15].contains("2"))
					writer.write("4;");
				if (Variables[15].contains("3"))
					writer.write("5;");
					
				if (Variables[16].contains("0"))
					writer.write("2;");
				if (Variables[16].contains("1"))
					writer.write("3;");
				if (Variables[16].contains("2"))
					writer.write("4;");
				if (Variables[16].contains("3"))
					writer.write("5;");
					
				if (Variables[17].contains("0"))
					writer.write("2;");
				if (Variables[17].contains("1"))
					writer.write("3;");
				if (Variables[17].contains("2"))
					writer.write("4;");
				if (Variables[17].contains("3"))
					writer.write("5;");
				
				//MTU = new Int(576, 1500);
				writer.write(Variables[18]+";");
				writer.write(Variables[19]+";");
				writer.write(Variables[20]+";");
				
				// defaultCallRate = new Int(64, 6000);
				writer.write(Variables[21]+";");
				writer.write(Variables[22]+";");
				writer.write(Variables[23]+";");
				
				// maxReceiveCallRate = new Int(64, 6000);
				writer.write(Variables[24]+";");
				writer.write(Variables[25]+";");
				writer.write(Variables[26]+";");
				
				// maxTransmitCallRate = new Int(64, 6000);
				writer.write(Variables[27]+";");
				writer.write(Variables[28]+";");
				writer.write(Variables[29]+";");
					
					// private String audioCodec = new (0,15) ; // {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}
				if (Variables[30].equals("10"))
					writer.write("GSM-8000;");
				if (Variables[30].equals("11"))
					writer.write("speex-8000;");
				if (Variables[30].equals("12"))
					writer.write("AMR-WB-16000;");
				if (Variables[30].equals("13"))
					writer.write("SILK-12000;");
				if (Variables[30].equals("14"))
					writer.write("SILK-8000;");
				if (Variables[30].equals("15"))
					writer.write("telephone-event-80000;");
				if (Variables[30].equals("0"))
					writer.write("Auto;");
				if (Variables[30].equals("1"))
					writer.write("opus-48000;");
				if (Variables[30].equals("2"))
					writer.write("SILK-24000;");
				if (Variables[30].equals("3"))
					writer.write("SILK-16000;");
				if (Variables[30].equals("4"))
					writer.write("G722-16000;");
				if (Variables[30].equals("5"))
					writer.write("speex-32000;");
				if (Variables[30].equals("6"))
					writer.write("speex-16000;");
				if (Variables[30].equals("7"))
					writer.write("PCMU-8000;");	
				if (Variables[30].equals("8"))
					writer.write("PCMA-8000;");
				if (Variables[30].equals("9"))
					writer.write("iLBC-8000;");
						
					
				if (Variables[31].equals("10"))
					writer.write("GSM-8000;");
				if (Variables[31].equals("11"))
					writer.write("speex-8000;");
				if (Variables[31].equals("12"))
					writer.write("AMR-WB-16000;");
				if (Variables[31].equals("13"))
					writer.write("SILK-12000;");
				if (Variables[31].equals("14"))
					writer.write("SILK-8000;");
				if (Variables[31].equals("15"))
					writer.write("telephone-event-80000;");	
				if (Variables[31].equals("0"))
					writer.write("Auto;");
				if (Variables[31].equals("1"))
					writer.write("opus-48000;");
				if (Variables[31].equals("2"))
					writer.write("SILK-24000;");
				if (Variables[31].equals("3"))
					writer.write("SILK-16000;");
				if (Variables[31].equals("4"))
					writer.write("G722-16000;");
				if (Variables[31].equals("5"))
					writer.write("speex-32000;");
				if (Variables[31].equals("6"))
					writer.write("speex-16000;");
				if (Variables[31].equals("7"))
					writer.write("PCMU-8000;");	
				if (Variables[31].equals("8"))
					writer.write("PCMA-8000;");
				if (Variables[31].equals("9"))
					writer.write("iLBC-8000;");
				
					
				if (Variables[32].equals("10"))
					writer.write("GSM-8000;");
				if (Variables[32].equals("11"))
					writer.write("speex-8000;");
				if (Variables[32].equals("12"))
					writer.write("AMR-WB-16000;");
				if (Variables[32].equals("13"))
					writer.write("SILK-12000;");
				if (Variables[32].equals("14"))
					writer.write("SILK-8000;");
				if (Variables[32].equals("15"))
					writer.write("telephone-event-80000;");
				if (Variables[32].equals("0"))
					writer.write("Auto;");
				if (Variables[32].equals("1"))
					writer.write("opus-48000;");
				if (Variables[32].equals("2"))
					writer.write("SILK-24000;");
				if (Variables[32].equals("3"))
					writer.write("SILK-16000;");
				if (Variables[32].equals("4"))
					writer.write("G722-16000;");
				if (Variables[32].equals("5"))
					writer.write("speex-32000;");
				if (Variables[32].equals("6"))
					writer.write("speex-16000;");
				if (Variables[32].equals("7"))
					writer.write("PCMU-8000;");	
				if (Variables[32].equals("8"))
					writer.write("PCMA-8000;");
				if (Variables[32].equals("9"))
					writer.write("iLBC-8000;");
	
					// private String videoCodec = new (0,5) ; // {Auto, h264,red,rtx,ulpfec,VP8}
				if (Variables[33].contains("0"))
					writer.write("Auto;");
				if (Variables[33].contains("1"))
					writer.write("h264;");
				if (Variables[33].contains("2"))
					writer.write("red;");
				if (Variables[33].contains("3"))
					writer.write("rtx;");
				if (Variables[33].contains("4"))
					writer.write("ulpfec;");
				if (Variables[33].contains("5"))
					writer.write("VP8;");
					
				if (Variables[34].contains("0"))
					writer.write("Auto;");
				if (Variables[34].contains("1"))
					writer.write("h264;");
				if (Variables[34].contains("2"))
					writer.write("red;");
				if (Variables[34].contains("3"))
					writer.write("rtx;");
				if (Variables[34].contains("4"))
					writer.write("ulpfec;");
				if (Variables[34].contains("5"))
					writer.write("VP8;");
					
				if (Variables[35].contains("0"))
					writer.write("Auto;");
				if (Variables[35].contains("1"))
					writer.write("h264;");
				if (Variables[35].contains("2"))
					writer.write("red;");
				if (Variables[35].contains("3"))
					writer.write("rtx;");
				if (Variables[35].contains("4"))
					writer.write("ulpfec;");
				if (Variables[35].contains("5"))
					writer.write("VP8;");	

					// private int maxResolution = new (0,4) ; //{1080,720,480,360,240}
				if (Variables[36].contains("0"))
					writer.write("1080;");
				if (Variables[36].contains("1"))
					writer.write("720;");
				if (Variables[36].contains("2"))
					writer.write("480;");
				if (Variables[36].contains("3"))
					writer.write("360;");
				if (Variables[36].contains("4"))
					writer.write("240;");
					
				if (Variables[37].contains("0"))
					writer.write("1080;");
				if (Variables[37].contains("1"))
					writer.write("720;");
				if (Variables[37].contains("2"))
					writer.write("480;");
				if (Variables[37].contains("3"))
					writer.write("360;");
				if (Variables[37].contains("4"))
					writer.write("240;");
					
				if (Variables[38].contains("0"))
					writer.write("1080");
				if (Variables[38].contains("1"))
					writer.write("720");
				if (Variables[38].contains("2"))
					writer.write("480");
				if (Variables[38].contains("3"))
					writer.write("360");
				if (Variables[38].contains("4"))
					writer.write("240");
				
				writer.write("\n");
			}
			writer.flush();
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    }
	
	public static void main(String[] args) throws IOException {
    	ReadData("VAR","VAR.csv") ;
	}
}
