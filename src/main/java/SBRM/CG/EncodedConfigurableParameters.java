package SBRM.CG;

import jmetal.util.JMException;
import jmetal.util.wrapper.XInt;

import java.util.Random;

public class EncodedConfigurableParameters {
    public int protocol;
    public int protocol_s2;
    public int protocol_s3;

    public int sIPListenPort;
    public int sIPListenPort_s2;
    public int sIPListenPort_s3;

    public int transportProtocol;
    public int transportProtocol_s2;
    public int transportProtocol_s3;

    public int encryption;
    public int encryption_s2;
    public int encryption_s3;

    public int sipZrtpAttribute;
    public int sipZrtpAttribute_s2;
    public int sipZrtpAttribute_s3;

    public int maxPeersAllowedInConferenceCall;
    public int maxPeersAllowedInConferenceCall_s2;
    public int maxPeersAllowedInConferenceCall_s3;

    public int MTU;
    public int MTU_s2;
    public int MTU_s3;

    public int defaultCallRate;
    public int defaultCallRate_s2;
    public int defaultCallRate_s3;

    public int maxReceiveCallRate;
    public int maxReceiveCallRate_s2;
    public int maxReceiveCallRate_s3;

    public int maxTransmitCallRate;
    public int maxTransmitCallRate_s2;
    public int maxTransmitCallRate_s3;

    public int audioCodec;
    public int audioCodec_s2;
    public int audioCodec_s3;

    public int videoCodec;
    public int videoCodec_s2;
    public int videoCodec_s3;

    public int maxResolution;
    public int maxResolution_s2;
    public int maxResolution_s3;

    public static int randInt(int min, int max) {
        Random rand = new Random(System.nanoTime()*System.currentTimeMillis());
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public EncodedConfigurableParameters getRandomConfiguration(){
        protocol = randInt(0, 6);
        protocol_s2 = randInt(0, 6);
        protocol_s3 = randInt(0, 6);

        sIPListenPort = randInt(0, 1);
        sIPListenPort_s2 = randInt(0, 1);
        sIPListenPort_s3 = randInt(0, 1);

        transportProtocol = randInt(0, 3);
        transportProtocol_s2 = randInt(0, 3);
        transportProtocol_s3 = randInt(0, 3);

        encryption = randInt(0, 2);
        encryption_s2 = randInt(0, 2);
        encryption_s3 = randInt(0, 2);

        sipZrtpAttribute = randInt(0, 2);
        sipZrtpAttribute_s2 = randInt(0, 2);
        sipZrtpAttribute_s3 = randInt(0, 2);

        maxPeersAllowedInConferenceCall = randInt(0, 3);
        maxPeersAllowedInConferenceCall_s2 = randInt(0, 3);
        maxPeersAllowedInConferenceCall_s3 = randInt(0, 3);

        MTU = randInt(576, 1500);
        MTU_s2 = randInt(576, 1500);
        MTU_s3 = randInt(576, 1500);

        defaultCallRate = randInt(64, 6000);
        defaultCallRate_s2 = randInt(64, 6000);
        defaultCallRate_s3 = randInt(64, 6000);

        maxReceiveCallRate = randInt(64, 6000);
        maxReceiveCallRate_s2 = randInt(64, 6000);
        maxReceiveCallRate_s3 = randInt(64, 6000);

        maxTransmitCallRate = randInt(64, 6000);
        maxTransmitCallRate_s2 = randInt(64, 6000);
        maxTransmitCallRate_s3 = randInt(64, 6000);

        audioCodec = randInt(0, 15);
        audioCodec_s2 = randInt(0, 15);
        audioCodec_s3 = randInt(0, 15);

        videoCodec = randInt(0, 5);
        videoCodec_s2 = randInt(0, 5);
        videoCodec_s3 = randInt(0, 5);

        maxResolution = randInt(0, 4);
        maxResolution_s2 = randInt(0, 4);
        maxResolution_s3 = randInt(0, 4);

        return this;
    }

    public int getProtocol() {
        return protocol;
    }

    public void setProtocol(int protocol) {
        this.protocol = protocol;
    }

    public int getProtocol_s2() {
        return protocol_s2;
    }

    public void setProtocol_s2(int protocol_s2) {
        this.protocol_s2 = protocol_s2;
    }

    public int getProtocol_s3() {
        return protocol_s3;
    }

    public void setProtocol_s3(int protocol_s3) {
        this.protocol_s3 = protocol_s3;
    }

    public int getSIPListenPort() {
        return sIPListenPort;
    }

    public void setSIPListenPort(int sIPListenPort) {
        this.sIPListenPort = sIPListenPort;
    }

    public int getSIPListenPort_s2() {
        return sIPListenPort_s2;
    }

    public void setSIPListenPort_s2(int sIPListenPort_s2) {
        this.sIPListenPort_s2 = sIPListenPort_s2;
    }

    public int getSIPListenPort_s3() {
        return sIPListenPort_s3;
    }

    public void setSIPListenPort_s3(int sIPListenPort_s3) {
        this.sIPListenPort_s3 = sIPListenPort_s3;
    }

    public int getTransportProtocol() {
        return transportProtocol;
    }

    public void setTransportProtocol(int transportProtocol) {
        this.transportProtocol = transportProtocol;
    }

    public int getTransportProtocol_s2() {
        return transportProtocol_s2;
    }

    public void setTransportProtocol_s2(int transportProtocol_s2) {
        this.transportProtocol_s2 = transportProtocol_s2;
    }

    public int getTransportProtocol_s3() {
        return transportProtocol_s3;
    }

    public void setTransportProtocol_s3(int transportProtocol_s3) {
        this.transportProtocol_s3 = transportProtocol_s3;
    }

    public int getEncryption() {
        return encryption;
    }

    public void setEncryption(int encryption) {
        this.encryption = encryption;
    }

    public int getEncryption_s2() {
        return encryption_s2;
    }

    public void setEncryption_s2(int encryption_s2) {
        this.encryption_s2 = encryption_s2;
    }

    public int getEncryption_s3() {
        return encryption_s3;
    }

    public void setEncryption_s3(int encryption_s3) {
        this.encryption_s3 = encryption_s3;
    }

    public int getSipZrtpAttribute() {
        return sipZrtpAttribute;
    }

    public void setSipZrtpAttribute(int sipZrtpAttribute) {
        this.sipZrtpAttribute = sipZrtpAttribute;
    }

    public int getSipZrtpAttribute_s2() {
        return sipZrtpAttribute_s2;
    }

    public void setSipZrtpAttribute_s2(int sipZrtpAttribute_s2) {
        this.sipZrtpAttribute_s2 = sipZrtpAttribute_s2;
    }

    public int getSipZrtpAttribute_s3() {
        return sipZrtpAttribute_s3;
    }

    public void setSipZrtpAttribute_s3(int sipZrtpAttribute_s3) {
        this.sipZrtpAttribute_s3 = sipZrtpAttribute_s3;
    }

    public int getMaxPeersAllowedInConferenceCall() {
        return maxPeersAllowedInConferenceCall;
    }

    public void setMaxPeersAllowedInConferenceCall(int maxPeersAllowedInConferenceCall) {
        this.maxPeersAllowedInConferenceCall = maxPeersAllowedInConferenceCall;
    }

    public int getMaxPeersAllowedInConferenceCall_s2() {
        return maxPeersAllowedInConferenceCall_s2;
    }

    public void setMaxPeersAllowedInConferenceCall_s2(int maxPeersAllowedInConferenceCall_s2) {
        this.maxPeersAllowedInConferenceCall_s2 = maxPeersAllowedInConferenceCall_s2;
    }

    public int getMaxPeersAllowedInConferenceCall_s3() {
        return maxPeersAllowedInConferenceCall_s3;
    }

    public void setMaxPeersAllowedInConferenceCall_s3(int maxPeersAllowedInConferenceCall_s3) {
        this.maxPeersAllowedInConferenceCall_s3 = maxPeersAllowedInConferenceCall_s3;
    }

    public int getMTU() {
        return MTU;
    }

    public void setMTU(int mTU) {
        MTU = mTU;
    }

    public int getMTU_s2() {
        return MTU_s2;
    }

    public void setMTU_s2(int mTU_s2) {
        MTU_s2 = mTU_s2;
    }

    public int getMTU_s3() {
        return MTU_s3;
    }

    public void setMTU_s3(int mTU_s3) {
        MTU_s3 = mTU_s3;
    }

    public int getDefaultCallRate() {
        return defaultCallRate;
    }

    public void setDefaultCallRate(int defaultCallRate) {
        this.defaultCallRate = defaultCallRate;
    }

    public int getDefaultCallRate_s2() {
        return defaultCallRate_s2;
    }

    public void setDefaultCallRate_s2(int defaultCallRate_s2) {
        this.defaultCallRate_s2 = defaultCallRate_s2;
    }

    public int getDefaultCallRate_s3() {
        return defaultCallRate_s3;
    }

    public void setDefaultCallRate_s3(int defaultCallRate_s3) {
        this.defaultCallRate_s3 = defaultCallRate_s3;
    }

    public int getMaxReceiveCallRate() {
        return maxReceiveCallRate;
    }

    public void setMaxReceiveCallRate(int maxReceiveCallRate) {
        this.maxReceiveCallRate = maxReceiveCallRate;
    }

    public int getMaxReceiveCallRate_s2() {
        return maxReceiveCallRate_s2;
    }

    public void setMaxReceiveCallRate_s2(int maxReceiveCallRate_s2) {
        this.maxReceiveCallRate_s2 = maxReceiveCallRate_s2;
    }

    public int getMaxReceiveCallRate_s3() {
        return maxReceiveCallRate_s3;
    }

    public void setMaxReceiveCallRate_s3(int maxReceiveCallRate_s3) {
        this.maxReceiveCallRate_s3 = maxReceiveCallRate_s3;
    }

    public int getMaxTransmitCallRate() {
        return maxTransmitCallRate;
    }

    public void setMaxTransmitCallRate(int maxTransmitCallRate) {
        this.maxTransmitCallRate = maxTransmitCallRate;
    }

    public int getMaxTransmitCallRate_s2() {
        return maxTransmitCallRate_s2;
    }

    public void setMaxTransmitCallRate_s2(int maxTransmitCallRate_s2) {
        this.maxTransmitCallRate_s2 = maxTransmitCallRate_s2;
    }

    public int getMaxTransmitCallRate_s3() {
        return maxTransmitCallRate_s3;
    }

    public void setMaxTransmitCallRate_s3(int maxTransmitCallRate_s3) {
        this.maxTransmitCallRate_s3 = maxTransmitCallRate_s3;
    }

    public int getAudioCodec() {
        return audioCodec;
    }

    public void setAudioCodec(int audioCodec) {
        this.audioCodec = audioCodec;
    }

    public int getAudioCodec_s2() {
        return audioCodec_s2;
    }

    public void setAudioCodec_s2(int audioCodec_s2) {
        this.audioCodec_s2 = audioCodec_s2;
    }

    public int getAudioCodec_s3() {
        return audioCodec_s3;
    }

    public void setAudioCodec_s3(int audioCodec_s3) {
        this.audioCodec_s3 = audioCodec_s3;
    }

    public int getVideoCodec() {
        return videoCodec;
    }

    public void setVideoCodec(int videoCodec) {
        this.videoCodec = videoCodec;
    }

    public int getVideoCodec_s2() {
        return videoCodec_s2;
    }

    public void setVideoCodec_s2(int videoCodec_s2) {
        this.videoCodec_s2 = videoCodec_s2;
    }

    public int getVideoCodec_s3() {
        return videoCodec_s3;
    }

    public void setVideoCodec_s3(int videoCodec_s3) {
        this.videoCodec_s3 = videoCodec_s3;
    }

    public int getMaxResolution() {
        return maxResolution;
    }

    public void setMaxResolution(int maxResolution) {
        this.maxResolution = maxResolution;
    }

    public int getMaxResolution_s2() {
        return maxResolution_s2;
    }

    public void setMaxResolution_s2(int maxResolution_s2) {
        this.maxResolution_s2 = maxResolution_s2;
    }

    public int getMaxResolution_s3() {
        return maxResolution_s3;
    }

    public void setMaxResolution_s3(int maxResolution_s3) {
        this.maxResolution_s3 = maxResolution_s3;
    }


    public EncodedConfigurableParameters intializeConfigurationInstance(XInt x) throws JMException {

        protocol = x.getValue(0);
        protocol_s2 = x.getValue(1);
        protocol_s3 = x.getValue(2);

        sIPListenPort = x.getValue(3);
        sIPListenPort_s2 = x.getValue(4);
        sIPListenPort_s3 = x.getValue(5);

        transportProtocol = x.getValue(6);
        transportProtocol_s2 = x.getValue(7);
        transportProtocol_s3 = x.getValue(8);

        encryption = x.getValue(9);
        encryption_s2 = x.getValue(10);
        encryption_s3 = x.getValue(11);

        sipZrtpAttribute = x.getValue(12);
        sipZrtpAttribute_s2 = x.getValue(13);
        sipZrtpAttribute_s3 = x.getValue(14);

        maxPeersAllowedInConferenceCall = x.getValue(15);
        maxPeersAllowedInConferenceCall_s2 = x.getValue(16);
        maxPeersAllowedInConferenceCall_s3 = x.getValue(17);

        MTU = x.getValue(18);
        MTU_s2 = x.getValue(19);
        MTU_s3 = x.getValue(20);

        defaultCallRate = x.getValue(21);
        defaultCallRate_s2 = x.getValue(22);
        defaultCallRate_s3 = x.getValue(23);

        maxReceiveCallRate = x.getValue(24);
        maxReceiveCallRate_s2 = x.getValue(25);
        maxReceiveCallRate_s3 = x.getValue(26);

        maxTransmitCallRate = x.getValue(27);
        maxTransmitCallRate_s2 = x.getValue(28);
        maxTransmitCallRate_s3 = x.getValue(29);

        audioCodec = x.getValue(30);
        audioCodec_s2 = x.getValue(31);
        audioCodec_s3 = x.getValue(32);

        videoCodec = x.getValue(33);
        videoCodec_s2 = x.getValue(34);
        videoCodec_s3 = x.getValue(35);

        maxResolution = x.getValue(36);
        maxResolution_s2 = x.getValue(37);
        maxResolution_s3 = x.getValue(38);

        return this;
    }
}




