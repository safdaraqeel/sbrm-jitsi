package SBRM.CG;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class DecodedConfigurableParameters
{
    private String protocol;    //{SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}
    private String sIPListenPort; // {On, Off}
    private String transportProtocol; // {Auto, UDP,TCP,TLS}
    private String encryption; // {On, Off, BestEffort}
    private String sipZrtpAttribute; //{Auto, true, false}
    private int maxPeersAllowedInConferenceCall; // {2,3,4,5}
    private int MTU;                    //{576...1500}
    private int defaultCallRate;        //{64...6000}
    private int maxReceiveCallRate;    //{64...6000}
    private int maxTransmitCallRate; //{64...6000}
    private String audioCodec;      // {Auto, opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}
    private String videoCodec;     // {Auto, h264,red,rtx,ulpfec,VP8}
    private int maxResolution;  //{1080,720,480,360,240}

    public String getsIPListenPort()
    {
        return sIPListenPort;
    }
    public void setsIPListenPort(String sIPListenPort)
    {
        this.sIPListenPort = sIPListenPort;
    }

    public static DecodedConfigurableParameters getDefaultConfigurations()
    {
        DecodedConfigurableParameters receiverConfiguration = new DecodedConfigurableParameters();
        receiverConfiguration.setAudioCodec("X");
        receiverConfiguration.setEncryption("BestEffort");
        receiverConfiguration.setMaxPeersAllowedInConferenceCall(3);
        receiverConfiguration.setSIPListenPort("On");
        receiverConfiguration.setTransportProtocol("UDP");
        receiverConfiguration.setVideoCodec("Y");
        receiverConfiguration.setSipZrtpAttribute("true");
      return receiverConfiguration;
    }

    public String getProtocol()
    {
        return protocol;
    }

    public void setProtocol(String protocol)
    {
        this.protocol = protocol;
    }

    public String getSIPListenPort()
    {
        return sIPListenPort;
    }

    public void setSIPListenPort(String sIPListenPort)
    {
        this.sIPListenPort = sIPListenPort;
    }

    public String getTransportProtocol()
    {
        return transportProtocol;
    }

    public void setTransportProtocol(String transportProtocol)
    {
        this.transportProtocol = transportProtocol;
    }

    public String getEncryption()
    {
        return encryption;
    }

    public void setEncryption(String encryption)
    {
        this.encryption = encryption;
    }

    public String getSipZrtpAttribute()
    {
        return sipZrtpAttribute;
    }

    public void setSipZrtpAttribute(String sipZrtpAttribute)
    {
        this.sipZrtpAttribute = sipZrtpAttribute;
    }

    public int getMaxPeersAllowedInConferenceCall()
    {
        return maxPeersAllowedInConferenceCall;
    }

    public void setMaxPeersAllowedInConferenceCall(
        int maxPeersAllowedInConferenceCall)
    {
        this.maxPeersAllowedInConferenceCall = maxPeersAllowedInConferenceCall;
    }

    public int getMTU()
    {
        return MTU;
    }

    public void setMTU(int mTU)
    {
        MTU = mTU;
    }

    public int getDefaultCallRate()
    {
        return defaultCallRate;
    }

    public void setDefaultCallRate(int defaultCallRate)
    {
        this.defaultCallRate = defaultCallRate;
    }

    public int getMaxReceiveCallRate()
    {
        return maxReceiveCallRate;
    }

    public void setMaxReceiveCallRate(int maxReceiveCallRate)
    {
        this.maxReceiveCallRate = maxReceiveCallRate;
    }

    public int getMaxTransmitCallRate()
    {
        return maxTransmitCallRate;
    }

    public void setMaxTransmitCallRate(int maxTransmitCallRate)
    {
        this.maxTransmitCallRate = maxTransmitCallRate;
    }

    public String getAudioCodec()
    {
        return audioCodec;
    }

    public void setAudioCodec(String audioCodec)
    {
        this.audioCodec = audioCodec;
    }

    public String getVideoCodec()
    {
        return videoCodec;
    }

    public void setVideoCodec(String videoCodec)
    {
        this.videoCodec = videoCodec;
    }

    public int getMaxResolution()
    {
        return maxResolution;
    }

    public void setMaxResolution(int maxResolution)
    {
        this.maxResolution = maxResolution;
    }

    public DecodedConfigurableParameters ReadConfigurationFromCSVFile(String peerIP){
        try {
            String newtext="";
            File file = new File("ConfigurationsForThreeSystem.csv");
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line = null;
            boolean readflag= false;
            while ((line = br.readLine()) != null) {
                String [] configurationArray =line.split(";");
                if (configurationArray[13].contains(peerIP) && configurationArray[14].contains("NotDone") && readflag==false){
  
                this.setProtocol(configurationArray[0]);    
                this.setSIPListenPort(configurationArray[1]); 
                this.setTransportProtocol(configurationArray[2]); 
                this.setEncryption(configurationArray[3]); 
                this.setSipZrtpAttribute(configurationArray[4]); 
                this.setMaxPeersAllowedInConferenceCall(Integer.parseInt(configurationArray[5])); 
                this.setMTU(Integer.parseInt(configurationArray[6]));
                this.setDefaultCallRate(Integer.parseInt(configurationArray[7]));
                this.setMaxReceiveCallRate(Integer.parseInt(configurationArray[8]));
                this.setMaxTransmitCallRate(Integer.parseInt(configurationArray[9]));
                this.setAudioCodec(configurationArray[10]); 
                this.setVideoCodec(configurationArray[11]); 
                this.setMaxResolution(Integer.parseInt(configurationArray[12]));
                line=line.replaceAll("NotDone", "Done");
                //System.out.println(line);
                readflag=true;
                }
                newtext+=line+"\n";
            }
            //System.out.println("\n\n\n"+newtext);
            FileWriter writer = new FileWriter("ConfigurationsForThreeSystem.csv");
            writer.write(newtext);
            writer.close();
            br.close();
        }
        catch (Exception e) {
        }
        return this;
    }

    public static void main(String[] args) {
        DecodedConfigurableParameters configuraiton = new DecodedConfigurableParameters();
        configuraiton.ReadConfigurationFromCSVFile("b");
       System.out.println(configuraiton.getAudioCodec());
    }

    @Override
    public String toString() {
        return "My_Configuration [desktopSharingSettings="
            + ", SIPListenPort=" + sIPListenPort + ", TransportProtocol="
            + ", encryption=" + encryption + ", sipZrtpAttribute="
            + sipZrtpAttribute + ", receivedCallType="
            + maxPeersAllowedInConferenceCall + ", MTU=" + MTU
            + ", defaultCallRate=" + defaultCallRate + ", MaxReceiveCallRate="
            + maxReceiveCallRate + ", MaxTransmitCallRate="
            + maxTransmitCallRate + ", audioCodec=" + audioCodec
            + ", videoCodec=" + videoCodec + ", maxResolution=" + maxResolution
            + "]";
    }
}
