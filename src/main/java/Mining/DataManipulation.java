package Mining;

import java.io.*;
import java.util.Scanner;

public class DataManipulation {

	public static void converCSV2ARFF(String sourceFilePath,String destinationFilePath){
		String header ="@relation Jitsi\n\n\n"+
				"@attribute callID1 string\n"+
				"@attribute callID2 string\n"+
				"@attribute Caller string\n"+
				"@attribute Peer1 string\n"+
				"@attribute Peer2 string\n"+
				"@attribute protocol {SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}\n"+
				"@attribute protocol_s2 {SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}\n"+
				"@attribute protocol_s3 {SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}\n"+
				"@attribute sIPListenPort {On, Off}\n"+
				"@attribute sIPListenPort_s2 {On, Off}\n"+
				"@attribute sIPListenPort_s3 {On, Off}\n"+
				"@attribute transportProtocol {Auto, UDP,TCP,TLS}\n"+
				"@attribute transportProtocol_s2 {Auto, UDP,TCP,TLS}\n"+
				"@attribute transportProtocol_s3 {Auto, UDP,TCP,TLS}\n"+
				"@attribute encryption {On, Off, BestEffort}\n"+
				"@attribute encryption_s2 {On, Off, BestEffort}\n"+
				"@attribute encryption_s3 {On, Off, BestEffort}\n"+
				"@attribute sipZrtpAttribute {Auto, TRUE, FALSE}\n"+
				"@attribute sipZrtpAttribute_s2 {Auto, TRUE, FALSE}\n"+
				"@attribute sipZrtpAttribute_s3 {Auto, TRUE, FALSE}\n"+
				"@attribute maxPeersAllowedInConferenceCall {2,3,4,5}\n"+
				"@attribute maxPeersAllowedInConferenceCall_s2 {2,3,4,5}\n"+
				"@attribute maxPeersAllowedInConferenceCall_s3 {2,3,4,5}\n"+
				"@attribute MTU numeric\n"+
				"@attribute MTU_s2 numeric\n"+
				"@attribute MTU_s3 numeric\n"+
				"@attribute defaultCallRate numeric\n"+
				"@attribute defaultCallRate_s2 numeric\n"+
				"@attribute defaultCallRate_s3 numeric\n"+
				"@attribute maxReceiveCallRate numeric\n"+
				"@attribute maxReceiveCallRate_s2 numeric\n"+
				"@attribute maxReceiveCallRate_s3 numeric\n"+
				"@attribute maxTransmitCallRate numeric\n"+
				"@attribute maxTransmitCallRate_s2 numeric\n"+
				"@attribute maxTransmitCallRate_s3 numeric\n"+
				"@attribute audioCodec  {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}\n"+
				"@attribute audioCodec_s2  {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}\n"+
				"@attribute audioCodec_s3  {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}\n"+
				"@attribute videoCodec {Auto, h264,red,rtx,ulpfec,VP8}\n"+
				"@attribute videoCodec_s2 {Auto, h264,red,rtx,ulpfec,VP8}\n"+
				"@attribute videoCodec_s3  {Auto, h264,red,rtx,ulpfec,VP8}\n"+
				"@attribute maxResolution {1080,720,480,360,240}\n"+
				"@attribute maxResolution_s2 {1080,720,480,360,240}\n"+
				"@attribute maxResolution_s3 {1080,720,480,360,240}\n"+
				"@attribute Call1status {Failed,Connected}\n"+
				"@attribute Call2status {Failed,Connected}\n"+
				"@attribute ConferenceCallstatus {FailedFailed,ConnectedConnected,FailedConnected,ConnectedFailed}\n\n"+
				"@data\n\n\n";
		String data="";

		File sourceFile = new File(sourceFilePath);
		File destinationFile = new File(destinationFilePath);
		try {
			Scanner scan = new Scanner(sourceFile);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				line=line.replaceAll(",", "").replaceAll("@", "").replaceAll(":", "").replaceAll("E+", "E");
				if (!line.toLowerCase().contains("encryption")){
				line=line.replaceAll(";", ",")+"\n";
				data+=line;
				}
			}
			
	    	// writing again
	    	FileWriter fw = new FileWriter(destinationFile,false);
	    	BufferedWriter bw = new BufferedWriter(fw);
	    	bw.write(header+data);
	    	bw.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
}
	
	
	public static void main(String[] args) {
		converCSV2ARFF("AllData.csv","AllData.arff");
	}
	
	
	
	

}
