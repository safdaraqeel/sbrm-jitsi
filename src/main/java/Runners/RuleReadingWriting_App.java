package Runners;

import SBRM.CPLRules.CPLRule;
import SBRM.CPLRules.ReadRules;
import SBRM.CPLRules.WriteRulesAndConfidence;

/**
 * Hello world!
 *
 */
public class RuleReadingWriting_App
{
    public static void main( String[] args )
    {
    	try{
    	ReadRules readRules= new ReadRules();
    	CPLRule [] rules= readRules.getRulesFromFile("constraints.txt");
    	WriteRulesAndConfidence writeRulesAndConfidence= new WriteRulesAndConfidence();
    	writeRulesAndConfidence.WriteRulesToExcel( "constraints.xlsx", rules,"sheet") ;
    	} catch (Exception e){
    		e.printStackTrace();
    	}
    }
}
